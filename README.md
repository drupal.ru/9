# Drupal.ru
Репозиторий кода для сайта русскоязычного сообщества Drupal.

[Развёртка локального окружения](https://gitlab.com/drupal.ru/8/issues/5)

Кросс-браузерное тестирование предоставлено
<a target="_blank" href="https://www.browserstack.com/"><img width="160" src="https://user-images.githubusercontent.com/34230/43679639-fe12efc4-9830-11e8-91eb-5bdab3c4c0fb.png" alt="BrowserStack" style="max-width:100%;"></a>
